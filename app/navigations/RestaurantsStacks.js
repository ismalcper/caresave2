import { createStackNavigator } from "react-navigation-stack";
import RestaurantsScreen from "../screens/Restaurants/Restaurants";
import AddRestaurantsScreen from "../screens/Restaurants/AddRestaurants";
import RestaurantScreen from "../screens/Restaurants/Restaurant";

const RestaurantsScreenStacks = createStackNavigator({
  Restaurants: {
    screen: RestaurantsScreen,
    navigationOptions: () => ({
      title: "Restaurantes",
    }),
  },
  Add: {
    screen: AddRestaurantsScreen,
    navigationOptions: () => ({
      title: "Añadir Restaurantes",
    }),
  },
  RestaurantInfo: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.item.name,
    }),
  },
});

export default RestaurantsScreenStacks;
