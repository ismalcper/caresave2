import { createStackNavigator } from "react-navigation-stack";
import TopListsScreen from "../screens/TopRestaurants";

const TopListsScreenStacks = createStackNavigator({
  TopList: {
    screen: TopListsScreen,
    navigationOptions: () => ({
      title: "Los mejores Restaurantes"
    })
  }
});

export default TopListsScreenStacks;
