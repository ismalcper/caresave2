import React, { useState } from "react";
import { SocialIcon } from "react-native-elements";
import * as firebase from "firebase";
import * as Facebook from "expo-facebook";
import { FacebookApi } from "../../utils/Social";
import Loading from "../Loading";

export default function LoginFacebook(props) {
  const { toastRef, navigation } = props;
  const [isVisibleLoading, setIsVisibleLoading] = useState(false);
  const Login = async () => {
    setIsVisibleLoading(true);
    try {
      await Facebook.initializeAsync(FacebookApi.aplication_id);
      const {
        type,
        token
      } = await Facebook.logInWithReadPermissionsAsync(
        FacebookApi.aplication_id,
        { permissions: FacebookApi.permissions }
      );

      if (type === "success") {
        const credentials = firebase.auth.FacebookAuthProvider.credential(
          token
        );
        await firebase
          .auth()
          .signInWithCredential(credentials)
          .then(() => {
            navigation.navigate("MyAccount");
          })
          .catch(() => {
            toastRef.current.show(
              "Error accediendo con Facebook, intentelo mas tarde"
            );
          });
      } else if (type === "cancel") {
        toastRef.current.show("Inicio de sesión cancelado");
      }
    } catch ({ message }) {
      console.log(message);
    }
    setIsVisibleLoading(false);
  };

  return (
    <>
      <SocialIcon
        title="Iniciar sesión con Facebook"
        button
        type="facebook"
        onPress={Login}
      />
      <Loading text="Iniciando sesión" isVisible={isVisibleLoading} />
    </>
  );
}
