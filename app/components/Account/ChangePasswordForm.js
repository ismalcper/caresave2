import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Icon } from "react-native-elements";
import * as firebase from "firebase";
import { reauthenticate } from "../../utils/Api";

export default function ChangePasswordForm(props) {
  const { setIsVisible, toastRef } = props;
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState(null);
  const [newPasswordRepeat, setNewPasswordRepeat] = useState(null);

  const [error, setError] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const [hidePassword, setHidePassword] = useState(true);
  const [hideNewPassword, setHideNewPassword] = useState(true);
  const [hideNewPasswordRepeat, setHideNewPasswordRepeat] = useState(true);

  const updatePassword = () => {
    setError({});
    if (!password || !newPassword || !newPasswordRepeat) {
      let objError = {};
      !password && (objError.password = "No puede estar vacio");
      !newPassword && (objError.newPassword = "No puede estar vacio");
      !newPasswordRepeat &&
        (objError.newPasswordRepeat = "No puede estar vacio");
      setError(objError);
    } else {
      if (password === newPassword) {
        setError({ password: "La contraseña no ha cambiado" });
      } else {
        if (newPassword !== newPasswordRepeat) {
          setError({
            newPassword: "La contraseña no es igual",
            newPasswordRepeat: "La contraseña no es igual"
          });
        } else {
          setIsLoading(true);
          reauthenticate(password)
            .then(() => {
              firebase
                .auth()
                .currentUser.updatePassword(newPassword)
                .then(() => {
                  setIsLoading(false);
                  toastRef.current.show("Contraseña actualizada con exito");
                  setIsVisible(false);
                  firebase.auth().signOut(); //salir de la cuenta
                })
                .catch(() => {
                  setError({ password: "Error al actualizar la contraseña" });
                  setIsLoading(false);
                });
            })
            .catch(() => {
              setError({ password: "La contraseña no es correcta" });
              setIsLoading(false);
            });
        }
      }
    }
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Contraseña Actual"
        containerStyle={styles.input}
        password={true}
        secureTextEntry={hidePassword}
        onChange={e => setPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hidePassword ? "eye-outline" : "eye-off-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHidePassword(!hidePassword)}
          />
        }
        errorMessage={error.password}
      />
      <Input
        placeholder="Nueva Contraseña"
        containerStyle={styles.input}
        password={true}
        secureTextEntry={hideNewPassword}
        onChange={e => setNewPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hideNewPassword ? "eye-outline" : "eye-off-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHideNewPassword(!hideNewPassword)}
          />
        }
        errorMessage={error.newPassword}
      />
      <Input
        placeholder="Repetir la Nueva Contraseña"
        containerStyle={styles.input}
        password={true}
        secureTextEntry={hideNewPasswordRepeat}
        onChange={e => setNewPasswordRepeat(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hideNewPasswordRepeat ? "eye-outline" : "eye-off-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHideNewPasswordRepeat(!hideNewPasswordRepeat)}
          />
        }
        errorMessage={error.newPasswordRepeat}
      />
      <Button
        title="Cambiar Contraseña"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updatePassword}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10
  },
  input: {
    marginBottom: 10,
    marginTop: 10
  },
  btnContainer: {
    marginTop: 20,
    width: "100%"
  },
  btn: {
    backgroundColor: "#00a680"
  },
  iconRight: {
    color: "#c1c1c1"
  }
});
