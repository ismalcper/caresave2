import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDCdUJLyX6ycdThw52jMP0UnvqQM3QlVgM",
  authDomain: "rest-3e7fa.firebaseapp.com",
  databaseURL: "https://rest-3e7fa.firebaseio.com",
  projectId: "rest-3e7fa",
  storageBucket: "rest-3e7fa.appspot.com",
  messagingSenderId: "592759459723",
  appId: "1:592759459723:web:e83351bb083aa26317298e"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
